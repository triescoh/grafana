# Grafana Dashboards for SSM

With Wizzy, a rich user-friendly command line tool written in node.js to manage Grafana entities, we are managing the Grafana Dashboards for SSM. It can also be used to store the entities like dashboards, rows, panels and even template variables in a Git repo making Grafana entities version controlled.

## Getting Started

First, install Wizzy here: https://utkarshcmu.github.io/wizzy-site/home/getting-started/

### Prerequisites

Read https://utkarshcmu.github.io/wizzy-site/home/getting-started/#prerequisites
```
yum install nodejs
```

### Installing

Once Wizzy installed and ready, clone the repository to the Wizzy directory.

```
git clone https://gitlab.cern.ch/triescoh/grafana.git
```

## Adding to Grafana

Get the json file with the version you want and follow the instruction here: http://docs.grafana.org/features/export_import/#import for importing into SSM 


## Authors

* **Tono Riesco** - *Initial work* - tono.riesco@cern.ch