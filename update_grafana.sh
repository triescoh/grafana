#!/bin/bash
cd /root/grafana/
git pull
wizzy import dashboards
wizzy import datasources
wizzy import orgs
git add .
git commit -a -m "$(date '+%Y-%m-%d %H:%M:%S')"
git push
